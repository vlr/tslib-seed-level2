import { spawn, spawnIt } from "@vlr/spawn";
import { projectPath, projectName } from "./projectName";

export async function cloneSeedToNewLibDir(): Promise<void> {
  await spawnIt("git clone", "git", ["clone", "git@gitlab.com:vlr/tslib-seed-level2.git", projectName()], { stdio: null });
}

export async function changeRemote(): Promise<void> {
  await spawn("git remote", "git", "remote", "set-url", "origin", `git@gitlab.com:${projectPath()}.git`);
  await spawn("git remote", "git", "remote", "add", "seed", `git@gitlab.com:vlr/tslib-seed-level2.git`);
}

export async function pushMaster(): Promise<void> {
  await spawnIt("git push", "git", ["push"], { stdio: null });
}

const branch = "feature/first-implementation";
export async function createBranch(): Promise<void> {
  await spawn("git branch", "git", "branch", branch);
  await spawn("git checkout", "git", "checkout", branch);
}

export async function pushChangesToBranch(): Promise<void> {
  await spawn("git commit", "git", "commit", "-a", "-m", `"first commit"`);
  await spawnIt("git push", "git", ["push", "--set-upstream", "origin", branch], { stdio: null });
}

export async function gitReset(): Promise<void> {
  await spawn("git reset", "git", "reset", "--hard");
}

export function wait(): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, 5000));
}
