import { parallel, series } from "gulp";
import { removeBuildFolder, tslint } from "./parts";
import { automate } from "../automate";

export const preparation = parallel(removeBuildFolder, series(automate, tslint));
